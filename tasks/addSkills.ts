import "@nomiclabs/hardhat-waffle"
import { task } from "hardhat/config"

task("addSkills")
  .addParam("tokenid", "the id of the NFT")
  .addParam("skilllist", "the list with skills to update a nft")
  .setAction(async ({ tokenid, skilllist }, hre) => {
    const { employer } = await hre.getNamedAccounts()
    console.log("running this adding skills task with account:", employer)

    const stringArray = skilllist.split(",")
    console.log("stringArray", stringArray)

    const unstoppableJobs = await hre.ethers.getContract(
      "UnstoppableJobs",
      employer
    )
    console.log(`Got contract UnstoppableJobs at ${unstoppableJobs.address}`)
    let transactionResponse
    console.log("updating skills...")
    console.log("tokenid", tokenid)
    console.log("tokenid", typeof tokenid)
    try {
      transactionResponse = await unstoppableJobs.updateSkills(
        tokenid,
        // ["partying", "coding"],
        stringArray,
        {
          gasLimit: 1000000,
        }
      )
      console.log("waiting...")
    } catch (error) {
      console.log("Error while updating skills", error)
      return
    }

    const txReceipt = await transactionResponse.wait(1)
    console.log("TX receipt:", txReceipt)
  })
