import "@nomiclabs/hardhat-waffle"
import { task } from "hardhat/config"
import { taikaiSkills } from "./../utils/taikai-skills"

task("mintAndAddSkills")
  .addParam("tokenuri", "The token URI of the NFT")
  .addParam("type", "type talent or job of the NFT")
  .setAction(async ({ tokenuri, type }, hre) => {
    // const randomNumOfSkills = Math.floor(Math.random() * 10) + 1
    const randomNumOfSkills = 10
    const amountOfTakaiSkills = taikaiSkills.data.technicalSkills.length
    console.log("amountOfTakaiSkills", amountOfTakaiSkills)
    console.log("randomNumOfSkills", randomNumOfSkills)

    let randomSkillArray: string[] = []
    for (let i = 0; i < randomNumOfSkills; i++) {
      const randomSkillIndex = Math.floor(Math.random() * amountOfTakaiSkills)
      const randomSkill =
        taikaiSkills.data.technicalSkills[randomSkillIndex].title
      if (!randomSkillArray.includes(randomSkill)) {
        const randomSkillName = randomSkillArray.push(randomSkill)
      }
    }

    const { employer } = await hre.getNamedAccounts()
    console.log("running this minting task with account:", employer)

    const unstoppableJobs = await hre.ethers.getContract(
      "UnstoppableJobs",
      employer
    )
    console.log(`Got contract UnstoppableJobs at ${unstoppableJobs.address}`)
    console.log("Minting NFT...")
    let transactionResponse
    try {
      transactionResponse = await unstoppableJobs.mintNFT(tokenuri, type, {
        gasLimit: 1000000,
      })
      console.log("waiting...")
    } catch (error) {
      console.log("Error while minting NFT", error)
      return
    }

    const txReceipt = await transactionResponse.wait(2)
    console.log("TX receipt:", txReceipt)

    console.log("Getting NFT ID...")
    const nftId = txReceipt.events[0].args[2].toNumber()
    console.log("NFT ID:", nftId)
    console.log("randomSkillArray", randomSkillArray)
    console.log("randomSkillArray", randomSkillArray.toString())

    const transactionResponse2 = await hre.run("addSkills", {
      tokenid: txReceipt.events[0].args[2].toString(),
      // tokenid: "9",
      skilllist: randomSkillArray.toString(),
    })
  })
