import "@nomiclabs/hardhat-waffle"
import { task } from "hardhat/config"

task("singleMint")
  .addParam("tokenuri", "The token URI of the NFT")
  .addParam("type", "type talent or job of the NFT")
  .setAction(async ({ tokenuri, type }, hre) => {
    const { employer } = await hre.getNamedAccounts()
    console.log("running this minting task with account:", employer)

    const unstoppableJobs = await hre.ethers.getContract(
      "UnstoppableJobs",
      employer
    )
    console.log(`Got contract UnstoppableJobs at ${unstoppableJobs.address}`)
    console.log("Minting NFT...")
    let transactionResponse
    try {
      transactionResponse = await unstoppableJobs.mintNFT(tokenuri, type, {
        gasLimit: 1000000,
      })
      console.log("waiting...")
    } catch (error) {
      console.log("Error while minting NFT", error)
      return
    }

    const txReceipt = await transactionResponse.wait(2)
    console.log("TX receipt:", txReceipt)
  })
