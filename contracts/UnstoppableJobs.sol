// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "hardhat/console.sol";

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/utils/Counters.sol";

contract UnstoppableJobs is ERC721URIStorage {
  using Counters for Counters.Counter;
  Counters.Counter private _tokenIds;

  mapping(uint256 => string) public tokenIDToType;
  mapping(uint256 => string[]) public tokenIDToSkills;
  mapping(address => string) public accountType;

  uint256[] public jobIDs;
  uint256[] public talentIDs;

  constructor() ERC721("UnstoppableJobs", "UJ") {}

  function mintNFT(string memory tokenURI, string memory typeNFT)
    public
    returns (uint256)
  {
    bool isNftJob = keccak256(abi.encodePacked(typeNFT)) ==
      keccak256(abi.encodePacked("job"));

    bool isNftTalent = keccak256(abi.encodePacked((typeNFT))) ==
        keccak256(abi.encodePacked(("talent")));

    bool isAccountTypeJob = keccak256(abi.encodePacked(accountType[msg.sender])) ==
        keccak256(abi.encodePacked(("job")));

    bool isAccountTypeUndefined = keccak256(abi.encodePacked(accountType[msg.sender])) ==
        keccak256(abi.encodePacked(("")));

    require(isNftJob || isNftTalent, "Type must be job or talent");

    require((isAccountTypeJob && isNftJob) || isAccountTypeUndefined, 
      "You already minted your profile."
    );

    if (keccak256(abi.encodePacked((accountType[msg.sender]))) == 
        keccak256(abi.encodePacked(("")))) {
      accountType[msg.sender] = typeNFT;
    }

    _tokenIds.increment();
    uint256 newItemId = _tokenIds.current();

    tokenIDToType[newItemId] = typeNFT;

    _safeMint(msg.sender, newItemId);
    _setTokenURI(newItemId, tokenURI);

    if (
      keccak256(abi.encodePacked((typeNFT))) ==
      keccak256(abi.encodePacked(("job")))
    ) {
      jobIDs.push(newItemId);
    } else {
      talentIDs.push(newItemId);
    }

    return newItemId;
  }

  function getSkills(uint256 tokenId) public view returns (string[] memory) {
    return tokenIDToSkills[tokenId];
  }

  function updateSkills(uint256 tokenId, string[] memory skills) public {
    require(
      msg.sender == ownerOf(tokenId),
      "You are not the owner of this NFT"
    );

    tokenIDToSkills[tokenId] = skills;
  }

  function updateTokenURI(uint256 tokenId, string memory tokenURI)
    public
    returns (bool)
  {
    require(
      msg.sender == ownerOf(tokenId),
      "You are not the owner of this NFT"
    );

    _setTokenURI(tokenId, tokenURI);
    return true;
  }

  function getAllNFTsFromAddress(address _address)
    public
    view
    returns (uint256[] memory ids, string[] memory uris)
  {
    ids = new uint256[](balanceOf(_address));
    uris = new string[](balanceOf(_address));
    uint256 counter = 0;
    for (uint256 i = 0; i < _tokenIds.current(); i++) {
      if (ownerOf(i + 1) == _address) {
        ids[counter] = i + 1;
        uris[counter] = tokenURI(i + 1);
        counter++;
      }
    }
    return (ids, uris);
  }

  function getJobIDs() public view returns (uint256[] memory) {
    return jobIDs;
  }

  function getTalentIDs() public view returns (uint256[] memory) {
    return talentIDs;
  }

  function matchJobWithTalent(uint256 jobId) public view returns (
    uint256[] memory ids, 
    string[] memory uris,
    uint256[] memory rating
  ) {
    ids = new uint256[](5);
    uris = new string[](5);
    rating = new uint256[](5);

    uint256 ratingTreshold = 0;

    // long growing array for each talent that is onboarded
    uint256[] memory talent = talentIDs;
    
    // jobs have on average 5 but max 10 skills defined
    string[] memory jobSkills = tokenIDToSkills[jobId]; 
    
    // number of skill matches for each talent 
    uint256[] memory talentMatches = new uint256[](talent.length);

    for (uint256 t = 0; t < talent.length; t++) {
      // talent has on average 5 but max 10 skills defined
      string[] memory talentSkills = tokenIDToSkills[talent[t]];
      for (uint256 ts = 0; ts < talentSkills.length; ts++) {

        for(uint256 js = 0; js < jobSkills.length; js++) {
          if(keccak256(abi.encodePacked(jobSkills[js])) == 
            keccak256(abi.encodePacked(talentSkills[ts]))) {
            talentMatches[t] += 1;
          }
        }
      }
      
      if(talentMatches[t] > ratingTreshold) {
        for (uint256 best = 0; best < 5; best++) {
          if (talentMatches[t] > rating[best]) {
            for (uint256 i = 4; i > best; --i) {
              rating[i] = rating[i - 1];
              ids[i] = ids[i - 1];
              uris[i] = uris[i - 1];
            }
            rating[best] = talentMatches[t];
            ids[best] = talent[t];
            uris[best] = tokenURI(talent[t]);
            break;
          }
        }
        ratingTreshold = rating[4];
      }
    }
  }

  function matchTalentWithJobs(uint256 talentId) public view returns (
    uint256[] memory ids, 
    string[] memory uris,
    uint256[] memory rating
  ) {
    ids = new uint256[](5);
    uris = new string[](5);
    rating = new uint256[](5);

    uint256 ratingTreshold = 0;

    // long growing array for each job that is added
    uint256[] memory jobs = jobIDs;
    
    // talent has on average 5 but max 10 skills defined
    string[] memory talentSkills = tokenIDToSkills[talentId]; 
    
    // number of skill matches for each job 
    uint256[] memory jobMatches = new uint256[](jobs.length);

    for (uint256 j = 0; j < jobs.length; j++) {
      // jobs have on average 5 but max 10 skills defined
      string[] memory jobSkills = tokenIDToSkills[jobs[j]];
      for (uint256 js = 0; js < jobSkills.length; js++) {
        for(uint256 ts = 0; ts < talentSkills.length; ts++) {
          if(keccak256(abi.encodePacked(talentSkills[ts])) == 
            keccak256(abi.encodePacked(jobSkills[js]))) {
            jobMatches[j] += 1;
          }
        }
      }
      
      if(jobMatches[j] > ratingTreshold) {
        for (uint256 best = 0; best < 5; best++) {
          if (jobMatches[j] > rating[best]) {
            for (uint256 i = 4; i > best; i--) {
              rating[i] = rating[i - 1];
              ids[i] = ids[i - 1];
              uris[i] = uris[i - 1];
            }
            rating[best] = jobMatches[j];
            ids[best] = jobs[j];
            uris[best] = tokenURI(jobs[j]);
            break;
          }
        }
        ratingTreshold = rating[4];
      }
    }
  }
}
