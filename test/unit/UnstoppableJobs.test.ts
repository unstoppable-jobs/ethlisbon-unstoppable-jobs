import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers"
import { assert, expect } from "chai"
import { deployments, ethers, network } from "hardhat"
import { developmentChains } from "../../helper-hardhat-config"
import { UnstoppableJobs } from "../../typechain-types"

!developmentChains.includes(network.name)
  ? describe.skip
  : describe("UnstoppableJobs", function () {
      let unstoppableJobs: UnstoppableJobs // Contract instance
      let deployer: SignerWithAddress // Account that deploys the contract
      let employer: SignerWithAddress // Account that creates jobs
      let talent: SignerWithAddress // Account that looks for jobs
      let accounts: SignerWithAddress[] // Accounts array

      beforeEach(async () => {
        if (!developmentChains.includes(network.name)) {
          throw "You need to be on a development chain to run tests"
        }
        accounts = await ethers.getSigners()
        deployer = accounts[0]
        employer = accounts[1]
        talent = accounts[2]
        await deployments.fixture(["all"])
        unstoppableJobs = await ethers.getContract("UnstoppableJobs")
      })

      describe("constructor", function () {
        it("the constructor succesfully sets the name", async () => {
          const name = await unstoppableJobs.name()
          assert.equal(name, "UnstoppableJobs")
        })
      })

      describe("testing the minting", function () {
        it("first the owner has no NFT", async () => {
          const balance = await unstoppableJobs.balanceOf(deployer.address)
          assert.equal(balance.toString(), "0")
        })

        it("The deployer mints a nft, has the NFT and has the balance", async () => {
          const typeJob = "talent"
          const balanceBefore = await unstoppableJobs.balanceOf(
            deployer.address
          )
          assert.equal(balanceBefore.toString(), "0")
          await unstoppableJobs.mintNFT("", typeJob)
          const balance = await unstoppableJobs.balanceOf(deployer.address)
          assert.equal(balance.toString(), "1")
          assert.equal(await unstoppableJobs.ownerOf(1), deployer.address)
        })
      })

      describe("testing updating the URI", function () {
        it("first the tokenURI is not set yet,", async () => {
          const typeNFT = "talent"
          await unstoppableJobs.mintNFT("", typeNFT)
          const uri = await unstoppableJobs.tokenURI(1)
          assert.equal(uri, "")
        })
        it("updating and retrieving a tokenURI", async () => {
          const typeJob = "talent"
          await unstoppableJobs.mintNFT("", typeJob)
          const tokenURI =
            "https://gateway.pinata.cloud/ipfs/QmXgxkawc34PpaL8YcS6cN7UWgozJFA9htE7jJbLMTfvFh"
          await unstoppableJobs.updateTokenURI(1, tokenURI)
          const uri = await unstoppableJobs.tokenURI(1)
          assert.equal(
            uri,
            "https://gateway.pinata.cloud/ipfs/QmXgxkawc34PpaL8YcS6cN7UWgozJFA9htE7jJbLMTfvFh"
          )
        })
      })

      describe("testing getting all NFTs from a certain account", function () {
        it("first the deployer has no NFTs", async () => {
          const balance = await unstoppableJobs.balanceOf(deployer.address)
          assert.equal(balance.toString(), "0")
        })

        it("the deployer mints 2 NFTs, and getAllNFTsFromAddress Now returns the correct array", async () => {
          const balanceBefore = await unstoppableJobs.balanceOf(
            deployer.address
          )
          assert.equal(balanceBefore.toString(), "0")
          // minting nft 1
          await unstoppableJobs.connect(deployer).mintNFT("uri1", "job")
          // someone else minting nft 2
          await unstoppableJobs.connect(talent).mintNFT("uri2", "talent")
          // minting nft 3
          await unstoppableJobs.connect(deployer).mintNFT("uri3", "job")

          const balance = await unstoppableJobs.balanceOf(deployer.address)
          assert.equal(balance.toString(), "2")
          assert.equal(await unstoppableJobs.ownerOf(1), deployer.address)
          assert.equal(await unstoppableJobs.ownerOf(3), deployer.address)

          const expectedOwnedIDs = ["1", "3"]
          const expectedOUris = ["uri1", "uri3"]

          const [ownedIDs, uris] = await unstoppableJobs.getAllNFTsFromAddress(
            deployer.address
          )
          assert.equal(ownedIDs.length, 2)
          assert.equal(ownedIDs[0].toString(), expectedOwnedIDs[0])
          assert.equal(ownedIDs[1].toString(), expectedOwnedIDs[1])
          assert.equal(uris[0], expectedOUris[0])
          assert.equal(uris[1], expectedOUris[1])
        })
      })

      describe("test adding skills to your NFT", function () {
        beforeEach(async () => {
          const typeJob = "talent"
          await unstoppableJobs.mintNFT("", typeJob)
        })
        it("first your NFT, does not contain any skills", async () => {
          let skills = await unstoppableJobs.getSkills("1")

          assert.equal(skills.length, 0)
        })

        it("someone else cannot add skills to your nft", async () => {
          await expect(
            unstoppableJobs
              .connect(accounts[2])
              .updateSkills("1", ["party", "coding"])
          ).to.be.revertedWith("You are not the owner of this NFT")
        })
        it("you can add skills to your NFT with update skills", async () => {
          await unstoppableJobs
            .connect(deployer)
            .updateSkills("1", ["party", "coding"])
        })
        it("You can get your list of skills with getSkills", async () => {
          await unstoppableJobs
            .connect(deployer)
            .updateSkills("1", ["party", "coding"])
          const skills = await unstoppableJobs.getSkills("1")
          assert.equal(skills.length, 2)
          assert.equal(skills[0], "party")
          assert.equal(skills[1], "coding")
        })
      })

      describe("test if the array of jobIDs and talentIDs get filled correctly", function () {
        it("first the jobIDs and talentIDs are empty", async () => {
          const jobIDs = await unstoppableJobs.getJobIDs()
          const talentIDs = await unstoppableJobs.getTalentIDs()
          assert.equal(jobIDs.length, 0)
          assert.equal(talentIDs.length, 0)
        })
      })

      it("adding a jobID and talentID to the array", async () => {
        let talentIDs = await unstoppableJobs.getTalentIDs()
        assert.equal(talentIDs.length, 0)
        await unstoppableJobs.connect(talent).mintNFT("", "talent")
        talentIDs = await unstoppableJobs.getTalentIDs()
        assert.equal(talentIDs.length, 1)
        assert.equal(talentIDs[0].toString(), "1")

        await unstoppableJobs.connect(employer).mintNFT("", "job")
        await unstoppableJobs.connect(employer).mintNFT("", "job")
        const jobIDs = await unstoppableJobs.getJobIDs()
        assert.equal(jobIDs.length, 2)
        assert.equal(jobIDs[0].toString(), "2")
      })

      it("after minting a talent NFT my account type should be talent", async () => {
        await unstoppableJobs.connect(talent).mintNFT("", "talent")
        const accountType = await unstoppableJobs.accountType(talent.address)
        assert.equal(accountType, "talent")
      })

      it("after minting a job NFT my account type should be job", async () => {
        await unstoppableJobs.connect(employer).mintNFT("", "job")
        const accountType = await unstoppableJobs.accountType(employer.address)
        assert.equal(accountType, "job")
      })

      describe("test adding skills to your NFT and skills to a job and matching them", function () {
        beforeEach(async () => {
          await unstoppableJobs.connect(employer).mintNFT("", "job")
          await unstoppableJobs.connect(talent).mintNFT("talent1", "talent")
          await unstoppableJobs
            .connect(accounts[3])
            .mintNFT("talent2", "talent")
          await unstoppableJobs
            .connect(accounts[4])
            .mintNFT("talent3", "talent")
          await unstoppableJobs
            .connect(accounts[5])
            .mintNFT("talent4", "talent")
          await unstoppableJobs
            .connect(accounts[6])
            .mintNFT("talent5", "talent")
          await unstoppableJobs
            .connect(accounts[7])
            .mintNFT("talent6", "talent")
          await unstoppableJobs
            .connect(accounts[8])
            .mintNFT("talent7", "talent")
        })

        it("A job can be matched with talent", async () => {
          await unstoppableJobs
            .connect(employer)
            .updateSkills("1", [
              "management",
              "coding",
              "gardening",
              "cooking",
              "testing",
            ])
          await unstoppableJobs
            .connect(talent)
            .updateSkills("2", ["partying", "coding", "testing"])
          await unstoppableJobs
            .connect(accounts[3])
            .updateSkills("3", ["cooking", "coding", "testing"])
          await unstoppableJobs
            .connect(accounts[4])
            .updateSkills("4", ["partying", "coding", "sleeping"])
          await unstoppableJobs
            .connect(accounts[5])
            .updateSkills("5", [
              "management",
              "coding",
              "gardening",
              "cooking",
              "testing",
            ])
          await unstoppableJobs
            .connect(accounts[6])
            .updateSkills("6", ["hacking", "partying", "testing"])
          await unstoppableJobs
            .connect(accounts[7])
            .updateSkills("7", ["partying", "cooking", "sleeping"])
          await unstoppableJobs
            .connect(accounts[8])
            .updateSkills("8", ["testing"])

          const [ids, uris, matching] = await unstoppableJobs
            .connect(talent)
            .matchJobWithTalent("1")

          // console.log(ids)
          // console.log(uris)
          // console.log(matching)

          assert.equal(ids[0].toString(), "5")
          assert.equal(ids[1].toString(), "3")
          assert.equal(ids[2].toString(), "2")
        })
      })

      describe("test adding skills to your NFT and skills to many job and jobs them", function () {
        beforeEach(async () => {
          await unstoppableJobs.connect(talent).mintNFT("talent1", "talent")
          await unstoppableJobs.connect(employer).mintNFT("job1", "job")
          await unstoppableJobs.connect(employer).mintNFT("job2", "job")
          await unstoppableJobs.connect(employer).mintNFT("job3", "job")
          await unstoppableJobs.connect(employer).mintNFT("job4", "job")
          await unstoppableJobs.connect(employer).mintNFT("job5", "job")
          await unstoppableJobs.connect(employer).mintNFT("job6", "job")
          await unstoppableJobs.connect(employer).mintNFT("job7", "job")
        })

        it("Talent can be matched with jobs", async () => {
          await unstoppableJobs
            .connect(talent)
            .updateSkills("1", [
              "management",
              "coding",
              "gardening",
              "cooking",
              "testing",
            ])
          await unstoppableJobs
            .connect(employer)
            .updateSkills("2", ["partying", "coding", "testing"])
          await unstoppableJobs
            .connect(employer)
            .updateSkills("3", ["cooking", "coding", "testing"])
          await unstoppableJobs
            .connect(employer)
            .updateSkills("4", ["partying", "coding", "sleeping"])
          await unstoppableJobs
            .connect(employer)
            .updateSkills("5", [
              "management",
              "coding",
              "gardening",
              "cooking",
              "testing",
            ])
          await unstoppableJobs
            .connect(employer)
            .updateSkills("6", ["hacking", "partying", "testing"])
          await unstoppableJobs
            .connect(employer)
            .updateSkills("7", ["partying", "cooking", "sleeping"])
          await unstoppableJobs.connect(employer).updateSkills("8", ["testing"])

          const [ids, uris, matching] = await unstoppableJobs
            .connect(talent)
            .matchTalentWithJobs("1")

          console.log(ids)
          console.log(uris)
          console.log(matching)

          assert.equal(ids[0].toString(), "5")
          assert.equal(ids[1].toString(), "3")
          assert.equal(ids[2].toString(), "2")
        })
      })
    })
