import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers"
import { assert } from "chai"
import { ethers, network } from "hardhat"
import { developmentChains } from "../../helper-hardhat-config"

import { log } from "console"
import { UnstoppableJobs } from "../../typechain-types"

developmentChains.includes(network.name)
  ? describe.skip
  : describe("UnstoppableJobs", function () {
      let unstoppableJobs: UnstoppableJobs // Contract instance
      let deployer: SignerWithAddress // Account that deploys the contract

      beforeEach(async function () {
        const accounts = await ethers.getSigners()
        deployer = accounts[0]
        unstoppableJobs = await ethers.getContract(
          "UnstoppableJobs",
          deployer.address
        )
      })

      it("it allows to mints a nft, has the NFT and has the balance", async () => {
        const typeJob = "talent"
        const balanceBefore = await unstoppableJobs.balanceOf(deployer.address)
        const tx = await unstoppableJobs.mintNFT("", typeJob)
        await tx.wait(2)
        const balanceAfter = await unstoppableJobs.balanceOf(deployer.address)
        log(balanceBefore)

        assert.equal(balanceBefore.add("1").toString(), balanceAfter.toString())
      })
    })
