import { HardhatRuntimeEnvironment } from "hardhat/types"
import { DeployFunction } from "hardhat-deploy/types"
import verify from "../utils/verify"
import { networkConfig, developmentChains } from "../helper-hardhat-config"

const deployUnstoppableJobs: DeployFunction = async function (
  hre: HardhatRuntimeEnvironment
) {
  // @ts-ignore
  const { getNamedAccounts, deployments, network } = hre
  const { deploy, log } = deployments
  const { deployer } = await getNamedAccounts()
  const chainId: number = network.config.chainId!

  log("----------------------------------------------------")
  log("Deploying UnstoppableJobs and waiting for confirmations...")
  const unstoppableJobs = await deploy("UnstoppableJobs", {
    from: deployer,
    // args: [],
    log: true,
    // we need to wait if on a live network so we can verify properly
    // waitConfirmations: networkConfig[chainId]?.blockConfirmations || 0,
  })
  log(`UnstoppableJobs deployed at ${unstoppableJobs.address}`)
  if (
    !developmentChains.includes(network.name) &&
    process.env.ETHERSCAN_API_KEY
  ) {
    log("Verifying UnstoppableJobs...")
    await verify(unstoppableJobs.address, [])
  }
}

export default deployUnstoppableJobs
deployUnstoppableJobs.tags = ["all", "unstoppableJobs"]
